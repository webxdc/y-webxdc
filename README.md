# y-webxdc 

> Webxdc provider for Yjs 

The `WebxdcProvider` turns apps using [yjs](https://docs.yjs.dev) 
into production-ready [webxdc apps](https://webxdc.org). 
Webxdc apps can be shared in a chat with messengers 
like [Delta Chat](https://delta.chat) and [Cheogram](https://cheogram.com),
sharing all application state with chat members via yjs. 

`WebxdcProvider` supports: 

- immediate receiving of yjs changes from chat peers 

- autosaving: saving yjs changes periodically to chat peers 

- manual saving: call `provider.syncToChatPeers()` for causing immediate saving
  (it's fine to mix manual and autosaving). 

- setting of document, summary and chat-message info 
  which is the metadata shown in a chat (see below screenshots). 

- reliably saving any pending yjs changes when the app window closes, 
  on all webxdc-supporting platforms. 
 
## Setup

### Install

```npm i y-webxdc```


### Client code

```js
import * as Y from 'yjs'
import { WebxdcProvider } from 'y-webxdc'

// provided by messengers or webxdc-dev tool 
// see https://docs.webxdc.org/spec.html
const webxdc = window.webxdc;  

const ydoc = new Y.Doc()
const yarray = ydoc.get('array', Y.Array)
const provider = new WebxdcProvider({
    webxdc,
    ydoc,
    autosaveInterval: 10*1000,
    getEditInfo: () => {
        const document = 'webxdc yjs provider';
        const summary = `Last edit: ${webxdc.selfName}`;
        const startinfo = `${webxdc.selfName} editing ${document}`;
        return {document, summary, startinfo}; 
    },
});
```

See the following example for the meaning of `document`, `summary` and `startinfo`
as returned by the `getEditInfo` callback passed into the provider. 

### Example 

The [webxdc editor](https://codeberg.org/webxdc/editor) uses `y-webxdc` 
to implement a collaborative editor. 

![Editor running Delta Chat desktop](https://codeberg.org/webxdc/y-webxdc/media/branch/main/doc/screenshot2.png)

![Showing edit information in chat](https://codeberg.org/webxdc/y-webxdc/media/branch/main/doc/screenshot.png)

## Development

### Test 

Run code-style checks with the following command (basically same as `npm run check`):

```
npm test
```

### Code Style

Code style is checked and fixed by [`eslint`](https://eslint.org/), run: 

```
npm run check
npm run fix
```

to perform checks or apply suggested fixes. 
Note that `npm test` will automatically check code style. 

