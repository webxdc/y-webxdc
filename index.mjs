import { applyUpdateV2, mergeUpdatesV2 } from 'yjs';
import { fromUint8Array, toUint8Array } from 'js-base64';


export default class WebxdcProvider {
    constructor({ webxdc, ydoc, getEditInfo, autosaveInterval }) {
        this.webxdc = webxdc;
        this.ydoc = ydoc;
        this.getEditInfo = getEditInfo;

        // queued yjs-updates, to be flushed and sent out in syncToChatPeers()
        this.queuedYjsUpdates = [];
        this.everNotifiedPeersAboutEdits = false;

        // call 'sync' handlers with {hasQueued: true|false} on queue changes
        this.eventHandlers = { sync: [] };
        this.on = (name, handler) => {
            this.eventHandlers[name].push(handler);
        };

        // setup automatic webxdc/yjs IO synchronization 
        webxdc.setUpdateListener(update => this.receiveWebxdcUpdateFromChatPeers(update));
        ydoc.on('updateV2', (yjsupdate, origin) => this.receiveYjsUpdate(yjsupdate, origin));
        registerExitHandlerForWebxdcWindow(() => this.syncToChatPeers());
        setInterval(() => this.syncToChatPeers(), autosaveInterval);
    }

    syncToChatPeers() {
        if (this.queuedYjsUpdates.length <= 0) {
            return;
        }
        const { document, summary, startinfo } = this.getEditInfo();
        const mergedYjsUpdate = mergeUpdatesV2(this.queuedYjsUpdates);
        const payload = { serializedYjsUpdate: fromUint8Array(mergedYjsUpdate) };
        const update = { payload, document, summary};
        if (!this.everNotifiedPeersAboutEdits) {
            update.info = startinfo;
            this.everNotifiedPeersAboutEdits = true;
        }
        this.webxdc.sendUpdate(update, 'document edited');
        this.queuedYjsUpdates.length = 0;
        this.eventHandlers.sync.map((func) => func({hasQueued: false}));
    }

    receiveWebxdcUpdateFromChatPeers(update) {
        const serialized = update.payload.serializedYjsUpdate;
        if (serialized) {
            const ydoc = this.ydoc;
            applyUpdateV2(ydoc, toUint8Array(serialized), ydoc.clientID);
        }
    }

    receiveYjsUpdate(yjsUpdate, origin) {
        if (origin === this.ydoc.clientID) {
            return;
        }
        this.queuedYjsUpdates.push(yjsUpdate);
        this.eventHandlers.sync.map((func) => func({hasQueued: true}));
    }
}


function registerExitHandlerForWebxdcWindow(finalize) {
    // On Android and iOS visibilitychange handlers are reliably called 
    window.addEventListener('visibilitychange', () => {
        if (document.visibilityState !== 'hidden') {
            return;
        }
        finalize();
    });

    // Desktop only executes beforeunload handlers on windows close. 
    window.addEventListener('beforeunload', sendUpdatesBeforeUnload);

    function sendUpdatesBeforeUnload(beforeUnloadEvent) {
    // Desktop does not execute webxdc.sendUpdate() synchronously, see
    // https://github.com/deltachat/deltachat-desktop/issues/3321#issue-1814659377
    // The workaround is to prevent closing in beforeunload handler and close
    // the window ourselves after a small timeout. 
        window.removeEventListener('beforeunload', sendUpdatesBeforeUnload);
        finalize();
        setTimeout(() => {
            // We close the parent, relying on running inside an `<iframe>`
            // which is the case for Delta Chat Desktop.
            try {
                window.parent.close();
            } catch (e) {
                window.close();
            }
        }, 100 /* milliseconds */ );

        beforeUnloadEvent.preventDefault();
        // For iOS Safari compatibility as of Sept 2023
        beforeUnloadEvent.returnValue = '';
        return '';
    }
}
